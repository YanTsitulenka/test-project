import {Component, OnInit} from '@angular/core';
import {User} from './models/user';
import {UsersNetworksService} from "./services/users-networks.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit {
  signIn: boolean;

  constructor( private usersNetworksService: UsersNetworksService,
               private router: Router) { }

  ngOnInit() {
    !this.signIn ? this.router.navigate(['/login']) : this.router.navigate(['/content']);
    this.usersNetworksService.eventEmitter.subscribe(
      res => {
        if (res) {
          this.signIn = res;
        }
      }
    );
  }
}
