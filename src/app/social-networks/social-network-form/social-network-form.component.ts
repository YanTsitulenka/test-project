import { EventEmitter, Output, Component, OnInit } from '@angular/core';
import { UsersNetworksService } from '../../services/users-networks.service';

@Component({
  selector: 'app-social-network-form',
  templateUrl: './social-network-form.component.html',
  styleUrls: []
})
export class SocialNetworkFormComponent implements OnInit {
  @Output() closeForm = new EventEmitter();
  socialNetworksStrings: Array<string>;
  selectedNetwork: string;
  selectedName: string;
  errorCheck = false;
  errorMessage = 'Fill in all the fields';

  constructor( private usersNetworksService: UsersNetworksService) { }

  ngOnInit() {
    this.socialNetworksStrings = this.usersNetworksService.getSocialNetworksList();
  }
  save(): void {
    if (!this.selectedNetwork || !this.selectedName) {
      this.errorCheck = true;
    } else {
      this.usersNetworksService.addSelectedSocialNetwork(this.selectedNetwork, this.selectedName);
      this.cancel();
    }
  }
  cancel(): void {
    this.selectedNetwork = '';
    this.selectedName = '';
    this.closeForm.emit();
  }

}
