import {EventEmitter, Injectable} from '@angular/core';
import { User } from '../models/user';
import { SocialNetwork } from '../models/social-network';
import { TasksService } from './tasks.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Constants } from './constants';
import 'rxjs/add/operator/toPromise';
import {SortService} from "./sort.service";

@Injectable()
export class UsersNetworksService {
  signInCheck = false;
  eventEmitter: EventEmitter<boolean> = new EventEmitter();
  headers = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });
  socialNetworksList = [
    'Vk',
    'Facebook',
    'LinkedIn',
    'Twitter'
  ];
  users = [];
  selectedSocialNetworks = [];
  currentUser: User;
  sortSettings = {
    sortNetwork: false,
    sortNetworkDesc: false,
    sortTask: false,
    sortTaskDesc: false
  };

  constructor( private tasks: TasksService,
               private http: Http,
               private sortService: SortService) {
    if ( localStorage.getItem('sortSettings') ) {
      this.sortSettings = JSON.parse(localStorage.getItem('sortSettings'));
    }
  }

  getCurrentUser(): User {
    return this.currentUser;
  }

  addSelectedSocialNetwork(network: string, name: string): void {
    this.selectedSocialNetworks.push(new SocialNetwork(network, name));
  }

  deleteSelectedSocialNetwork(network: SocialNetwork): void {
    this.tasks.deleteTasks(network);
    this.selectedSocialNetworks.splice(this.selectedSocialNetworks.indexOf(network), 1);
  }

  getSelectedSocialNetworks(): Promise<SocialNetwork[]> {
    return Promise.resolve(this.selectedSocialNetworks);
  }

  getSocialNetworksList(): string[] {
    return this.socialNetworksList;
  }

  signIn(login: string, password: string = null): Promise<boolean> {
    return this.http.post(
      Constants.USERS_URL + '/user/' + login + '/login',
      JSON.stringify({
        'login' : login,
        'password' : password
      }),
      this.options
    )
      .toPromise()
      .then(
        res => {
          if ( res.text() ) {
            const user = res.json();
            this.currentUser = new User(user.login, user.password, user.name);
            this.signInCheck = true;
            this.eventEmitter.emit(true);
            return true;
          }
          return false;
        }
      );
  }

  signUp(login: string, password: string, name: string): Promise<boolean> {
    return this.http.post(
      Constants.USERS_URL + '/user/new',
      JSON.stringify({
        'login' : login,
        'password' : password,
        'name' : name
      }),
      this.options
    )
      .toPromise()
      .then(
        res => {
          if ( res.text() ) {
           return this.signIn(login, password);
          }
          return false;
        }
      );
  }

  signOut(): void {
    this.http.post(
      Constants.USERS_URL + '/user/' + this.currentUser.login + '/logout',
      JSON.stringify({
        'login' : this.currentUser.login,
        'password' : this.currentUser.password
      }),
      this.options
    )
      .toPromise()
      .then(
        res => {
          if ( res.text() ) {
            this.currentUser = null;
            this.signInCheck = false;
            this.eventEmitter.emit(false);
          }
        }
      );
  }

  sort(): void {
    this.selectedSocialNetworks = this.sortService.sort(this.selectedSocialNetworks);
  }

  customSort(idArray: Array<number>): void {
    this.selectedSocialNetworks = this.sortService.customSort(this.selectedSocialNetworks, idArray);
  }
}
