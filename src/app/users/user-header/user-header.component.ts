import { Component, OnInit } from '@angular/core';
import {UsersNetworksService} from '../../services/users-networks.service';

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: []
})
export class UserHeaderComponent implements OnInit {
  username = 'Guest';
  signInCheck = false;

  constructor( private usersNetworksService: UsersNetworksService ) { }

  ngOnInit() {
    this.usersNetworksService.eventEmitter.subscribe(
      res => {
        if ( res ) {
          this.username = this.usersNetworksService.getCurrentUser().name;
        }
        this.signInCheck = res;
      }
    );
  }

  signOut() {
    this.usersNetworksService.signOut();
  }

}
