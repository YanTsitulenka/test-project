import { Component, OnInit } from '@angular/core';
import { Task } from '../../models/task';
import { TasksService } from '../../services/tasks.service';
import { User } from '../../models/user';
import { UsersNetworksService } from '../../services/users-networks.service';
import Sortable  from 'sortablejs';
import $ from "jquery";


@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: []
})
export class TasksListComponent implements OnInit {
  socialNetworks = [];
  tasks: Array<Task>;
  edited: boolean;
  user: User;
  customSortCheck = false;

  constructor(
    private usersNetworksService: UsersNetworksService,
    private tasksService: TasksService) { }

  ngOnInit() {
    this.usersNetworksService.getSelectedSocialNetworks()
      .then(socialNetworks => this.socialNetworks = socialNetworks)
      .catch((err) => console.log(err));
    this.user = this.usersNetworksService.getCurrentUser();
    this.tasksService.getTasks(this.user)
      .then(tasks => this.tasks = tasks)
      .catch( (err) => console.log(err));
    Sortable.create(document.getElementById('tasks'), {
      handle: '.buttons-move',
      animation: 150,
    });
  }

  showForm(): void {
    this.edited = !this.edited;
  }

  deleteTask(task: Task): void {
    this.tasksService.deleteTask(task);
  }

  sort(): void {
    this.tasksService.sort();
  }

  customSort(): void {
    $('.task__buttons > .buttons-edit').fadeToggle();
    $('.task__buttons > .buttons-move').fadeToggle();
    $('.add-task').slideToggle();
    this.customSortCheck = !this.customSortCheck;
  }

  saveCustomSort():void {
    this.customSort();
    let idArray = [];
    $('app-task').each( (i, val) =>  idArray.push($(val).data('id')) );
    this.tasksService.customSort(idArray);
  }
}
