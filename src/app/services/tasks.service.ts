import {Injectable, EventEmitter} from '@angular/core';
import { Task } from '../models/task';
import { SocialNetwork } from '../models/social-network';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Constants } from './constants';
import 'rxjs/add/operator/toPromise';
import {User} from '../models/user';
import {SortService} from "./sort.service";

@Injectable()
export class TasksService {
  headers = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });
  tasks = [];
  user: User;

  constructor ( private http: Http,
                private sortService: SortService) { }

  getTasks(user: User): Promise<Task[]> {
    this.user = user;
    this.tasks = [];

    return this.http.get(
      Constants.TASKS_URL + '/' + user.login + '/task/all'
    )
      .toPromise()
      .then(
        res => {
          if (res.text()) {
            const tasksArray = res.json();
            tasksArray.forEach(el => this.tasks.push(new Task(el.id, el.name, el.socialId, el.userId, el.message)));
            this.tasks = this.sortService.initializationSort(this.tasks);
            return this.tasks;
          }
        }
      );
  }

  getTask(name: string): Task {
    return this.tasks.filter( el => el.name === name)[0];
  }

  deleteTask(task: Task): void {
    this.http.delete(
      Constants.TASKS_URL + '/' + this.user.login + '/task/' + task.id
    )
      .toPromise()
      .then(
        res => {
          if (res.text()) {
            this.tasks.splice(this.tasks.indexOf(task), 1);
          }
        }
      );
  }

  addTask(name: string, message: string, socialNetwork: SocialNetwork): void {
    this.http.put(
      Constants.TASKS_URL + '/' + this.user.login + '/task/new',
      JSON.stringify({
        name: name,
        socialId: socialNetwork.name,
        userId: this.user.login,
        message: message
      }),
      this.options
    )
      .toPromise()
      .then(
        res => {
          if (res.text()) {
            const obj = res.json();
            this.tasks.push(new Task(obj.id, obj.name, obj.socialId, obj.userId, obj.message));
            console.log(this.tasks);
          }
        }
      );

  }

  deleteTasks(network: SocialNetwork): void {
    this.tasks.forEach( el => {
      if (el.socialId === network.name) {
        this.deleteTask(el);
      }
    });
  }

  sort(): void {
    this.tasks = this.sortService.sort(this.tasks);
  }

  customSort(idArray: Array<number>):void {
    this.tasks = this.sortService.customSort(this.tasks, idArray);
  }
}
