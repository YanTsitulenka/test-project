import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UsersNetworksService } from '../../services/users-networks.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: []
})
export class UserRegistrationFormComponent implements OnInit {
  @Output() actionToggle = new EventEmitter();

  errorCheck = false;
  errorMessage: string;
  login: string;
  name: string;
  password: string;
  passwordCheck: string;
  signUpCheck = false;

  constructor( private usersNetworksService: UsersNetworksService,
               private router: Router) { }

  ngOnInit() {
  }

  signUp(): void {
    if (this.password !== this.passwordCheck) {
      this.errorCheck = true;
      this.errorMessage = 'Passwords do not match';
      this.password = '';
      this.passwordCheck = '';
    } else if (!this.password || !this.name || !this.login) {
      this.errorCheck = true;
      this.errorMessage = 'Fill in all the fields';
    } else {
      this.usersNetworksService.signUp(this.login, this.password, this.name)
        .then(res => {
          this.signUpCheck = res;
          if ( res ) {
            this.router.navigate(['/content']).catch( err => console.log(err) );
          }
        })
        .catch(err => console.log(err));
    }
  }
}
