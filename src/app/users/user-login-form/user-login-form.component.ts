import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsersNetworksService } from '../../services/users-networks.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-login-form',
  templateUrl: './user-login-form.component.html',
  styleUrls: []
})
export class UserLoginFormComponent implements OnInit {
  @Output() actionToggle = new EventEmitter();
  login: string;
  password: string;
  errorCheck = false;
  errorMessage: string;

  constructor( private usersNetworksService: UsersNetworksService,
               private router: Router) { }

  ngOnInit() {
  }

  signIn(): void {
    if (!this.password || !this.login) {
      this.errorCheck = true;
      this.errorMessage = 'Fill in all the fields';
    } else {
      this.usersNetworksService.signIn(this.login, this.password)
        .then(res => {
          if (!res) {
            this.errorCheck = true;
            this.login = '';
            this.password = '';
            this.errorMessage = 'Wrong login or password';
          } else {
            this.router.navigate(['/content']).catch( err => console.log(err) );
          }
        })
        .catch(err => console.log(err));
    }
  }

  changeAction(): void {
    this.actionToggle.emit();
  }
}
