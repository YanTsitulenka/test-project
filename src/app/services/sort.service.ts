import {Injectable} from '@angular/core';
import {Task} from "../models/task";

@Injectable()
export class SortService {
  sortSettings = {
    tasksSort: false,
    tasksSortDesc: false,
    tasksCustomSort: false,
    tasksCustomSortArray: [],

    socialNetworksSort: false,
    socialNetworksSortDesc: false,
    socialNetworksCustomSort: false,
    socialNetworksCustomSortArray: []
  };

  constructor () {
    if (localStorage.getItem('sortSettings')) {
      this.sortSettings = JSON.parse(localStorage.getItem('sortSettings'));
    } else {
      this.setInLocalStorage();
    }
  }

  sort(array: any): any {
    if (array[0] instanceof Task) {
      this.sortSettings.tasksCustomSort = false;
      this.sortSettings.tasksCustomSortArray = [];
      this.sortSettings.tasksSort = true;

      this.setInLocalStorage();

      this.sortSettings.tasksSortDesc = ! this.sortSettings.tasksSortDesc;

      return !this.sortSettings.tasksSortDesc ?
        array.sort(this.sortComparator).reverse() :
        array.sort(this.sortComparator);


    } else {
      this.sortSettings.socialNetworksCustomSort = false;
      this.sortSettings.socialNetworksCustomSortArray = [];
      this.sortSettings.socialNetworksSort = true;

      this.setInLocalStorage();

      this.sortSettings.socialNetworksSortDesc = ! this.sortSettings.socialNetworksSortDesc;

      return !this.sortSettings.socialNetworksSortDesc ?
        array.sort(this.sortComparator).reverse() :
        array.sort(this.sortComparator);
    }
  }

  initializationSort(array: any): any {
    if (array[0] instanceof Task) {
      if (this.sortSettings.tasksCustomSort) {
        this.customSort(array, this.sortSettings.tasksCustomSortArray);
      } else if(this.sortSettings.tasksSort) {
        this.sort(array);
      }
    } else {
      if (this.sortSettings.socialNetworksCustomSort) {
        return this.customSort(array, this.sortSettings.socialNetworksCustomSortArray);
      } else if(this.sortSettings.socialNetworksSort) {
        this.sort(array);
      }
    }
    return array;
  }


  sortComparator(a: any, b: any): number {
    return  a.name > b.name ? 1 : -1;
  }

  customSort(array: any, idArray: Array<number>): any {
   /* if (array[0] instanceof Task) {
      this.sortSettings.tasksSort = false;
      this.sortSettings.tasksSortDesc = false;
      this.sortSettings.tasksCustomSort = true;
      this.sortSettings.tasksCustomSortArray = idArray;
    } else {
      this.sortSettings.socialNetworksSort = false;
      this.sortSettings.socialNetworksSortDesc = false;
      this.sortSettings.socialNetworksCustomSort = true;
      this.sortSettings.socialNetworksCustomSortArray = idArray;
    }

    this.setInLocalStorage();

    let tasks = [];
    idArray.forEach( el => tasks.push(array[el]));
    return tasks;*/
   // Sorry, I tried
   return array;
  }

  setInLocalStorage(): void {
    localStorage.setItem('sortSettings', JSON.stringify(this.sortSettings));
  }
}
