export class Constants {
  public static SERVER_URL = 'http://94.176.232.220';
  public static USERS_PORT = '9005';
  public static TASKS_PORT = '9000';
  public static USERS_URL = Constants.SERVER_URL + ':' + Constants.USERS_PORT;
  public static TASKS_URL = Constants.SERVER_URL + ':' + Constants.TASKS_PORT;
}
