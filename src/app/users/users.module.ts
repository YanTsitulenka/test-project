import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersNetworksService } from '../services/users-networks.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [],
  providers: [
    UsersNetworksService
  ]
})
export class UsersModule { }
