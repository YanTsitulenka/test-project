import { SocialNetwork } from './social-network';

export class Task {
  constructor(
    public id: string,
    public name: string,
    public socialId: string,
    public userId: string,
    public message: string
  ) { }
}
