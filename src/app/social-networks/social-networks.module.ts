import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksService } from '../services/tasks.service';
import { UsersNetworksService } from '../services/users-networks.service';
import {SortService} from "../services/sort.service";

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    UsersNetworksService,
    TasksService,
    SortService
  ]
})
export class SocialNetworksModule { }
