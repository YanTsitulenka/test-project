import { Input, Output, EventEmitter, Component, OnInit } from '@angular/core';

import { SocialNetwork } from './../../models/social-network';
import {Router} from "@angular/router";

@Component({
  selector: 'app-social-network',
  templateUrl: './social-network.component.html',
  styleUrls: []
})
export class SocialNetworkComponent implements OnInit {
  @Input()  network: SocialNetwork;
  @Output() deleteSocialNetwork = new EventEmitter<SocialNetwork>();
  constructor( private router: Router ) { }

  ngOnInit() {
  }

  deleteNetwork(): void {
    this.deleteSocialNetwork.emit(this.network);
  }

  changeSocialNetwork(): void {
    this.router.navigate(['/social-network', this.network.name]).catch( err => console.log(err));
  }
}
