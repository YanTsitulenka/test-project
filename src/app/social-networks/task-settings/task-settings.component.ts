import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Task } from '../../models/task';
import { TasksService } from '../../services/tasks.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-settings',
  templateUrl: './task-settings.component.html',
  styleUrls: []
})
export class TaskSettingsComponent implements OnInit {
  task: Task;

  constructor( private tasksService: TasksService,
               private location: Location,
               private router: Router) { }

  ngOnInit() {
    this.task = this.tasksService.getTask(this.router.url.split('/').pop());
    if ( !this.task ) {
      this.router.navigate(['**']).catch( err => console.log(err) );
    }
  }

  back() {
    this.location.back();
  }
}
