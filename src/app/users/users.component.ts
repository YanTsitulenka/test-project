import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: []
})
export class UsersComponent implements OnInit {
  registrationCheck = false;

  constructor() { }

  ngOnInit() {
  }

  actionToggle(): void {
    this.registrationCheck = !this.registrationCheck;
  }
}
