import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { SocialNetworksComponent } from './social-networks/social-networks.component';
import { SocialNetworksModule } from './social-networks/social-networks.module';
import { SocialNetworkComponent } from './social-networks/social-network/social-network.component';
import { SocialNetworkListComponent } from './social-networks/social-network-list/social-network-list.component';
import { SocialNetworkFormComponent } from './social-networks/social-network-form/social-network-form.component';
import { SocialNetworkSettingsComponent } from './social-networks/social-network-settings/social-network-settings.component';

import { TaskComponent } from './social-networks/task/task.component';
import { TasksListComponent } from './social-networks/tasks-list/tasks-list.component';
import { TaskFormComponent} from './social-networks/task-form/task-form.component';
import { TaskSettingsComponent } from './social-networks/task-settings/task-settings.component';

import { UsersComponent} from './users/users.component';
import { UserHeaderComponent } from './users/user-header/user-header.component';
import { UserLoginFormComponent } from './users/user-login-form/user-login-form.component';
import { UserRegistrationFormComponent } from './users/user-registration-form/user-registration-form.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { UsersModule } from "./users/users.module";


const appRoutes: Routes = [
  { path: '', component: AppComponent},
  { path: 'login', component: UserLoginFormComponent},
  { path: 'registration', component: UserRegistrationFormComponent},
  { path: 'content', component: SocialNetworksComponent},
  { path: 'task/:id', component: TaskSettingsComponent},
  { path: 'social-network/:id', component: SocialNetworkSettingsComponent},
  { path: 'profile', component: UserProfileComponent},
  { path: '**', component: NotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,

    SocialNetworksComponent,
    SocialNetworkComponent,
    SocialNetworkListComponent,
    SocialNetworkFormComponent,
    SocialNetworkSettingsComponent,

    TaskComponent,
    TasksListComponent,
    TaskFormComponent,
    TaskSettingsComponent,

    UsersComponent,
    UserHeaderComponent,
    UserLoginFormComponent,
    UserRegistrationFormComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    SocialNetworksModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    UsersModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
