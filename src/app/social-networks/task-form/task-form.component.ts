import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import {SocialNetwork} from '../../models/social-network';
import {TasksService} from '../../services/tasks.service';
import {UsersNetworksService} from '../../services/users-networks.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: []
})
export class TaskFormComponent implements OnInit {
  @Output() closeForm = new EventEmitter();
  name: string;
  selectedSocialNetwork: string;
  message: string;
  socialNetworks: Array<SocialNetwork>;
  errorCheck = false;
  errorMessage = 'Fill in all the fields';

  constructor(
    private usersNetworksArrayService: UsersNetworksService,
    private tasksService: TasksService) { }

  ngOnInit() {
    this.usersNetworksArrayService.getSelectedSocialNetworks()
      .then(socialNetworks => this.socialNetworks = socialNetworks)
      .catch((err) => console.log(err));
  }
  save(): void {
    if (!this.selectedSocialNetwork || !this.name || !this.message) {
      this.errorCheck = true;
    } else {
      this.tasksService.addTask(this.name, this.message, this.socialNetworks.filter( el => el.name === this.selectedSocialNetwork )[0]);
      this.cancel();
    }
  }
  cancel(): void {
    this.name = null;
    this.message = null;
    this.selectedSocialNetwork = null;
    this.closeForm.emit();
  }
}
