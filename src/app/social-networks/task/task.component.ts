import { Input, Output, Component, OnInit, EventEmitter } from '@angular/core';
import { Task } from '../../models/task';
import {Router} from "@angular/router";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: []
})
export class TaskComponent implements OnInit {
  @Input()  task: Task;
  @Output() deleteTask = new EventEmitter<Task>();
  constructor( private router: Router) { }

  ngOnInit() {
  }

  deleteThisTask(): void {
    this.deleteTask.emit(this.task);
  }

  changeTask(): void {
    this.router.navigate(['/task', this.task.name]).catch( err => console.log(err));
  }
}
