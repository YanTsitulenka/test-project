import { Component, OnInit } from '@angular/core';
import { SocialNetwork } from '../../models/social-network';
import { UsersNetworksService } from '../../services/users-networks.service';
import Sortable  from 'sortablejs';
import $ from "jquery";

@Component({
  selector: 'app-social-network-list',
  templateUrl: './social-network-list.component.html',
  styleUrls: []
})
export class SocialNetworkListComponent implements OnInit {
  socialNetworks = [];
  edited = false;
  customSortCheck = false;

  constructor(
    private usersNetworksService: UsersNetworksService) { }

  ngOnInit() {
    this.usersNetworksService.getSelectedSocialNetworks()
      .then(socialNetworks => this.socialNetworks = socialNetworks)
      .catch((err) => console.log(err));
    Sortable.create(document.getElementById('networks'), {
      handle: '.buttons-move',
      animation: 150,
    });
  }

  deleteNetwork(network: SocialNetwork): void {
    this.usersNetworksService.deleteSelectedSocialNetwork(network);
  }

  showForm(): void {
    this.edited = !this.edited;
  }

  sort(): void{
    this.usersNetworksService.sort();
  }

  customSort(): void {
    $('.buttons-edit.sn').fadeToggle();
    $('.buttons-move.sn').fadeToggle();
    $('.add-network').slideToggle();
    this.customSortCheck = !this.customSortCheck;
  }

  saveCustomSort():void {
    this.customSort();
    let idArray = [];
    $('app-social-network').each( (i, val) =>  idArray.push($(val).data('id')) );
    this.usersNetworksService.customSort(idArray);
  }
}
