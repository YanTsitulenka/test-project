import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UsersNetworksService } from '../../services/users-networks.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: []
})
export class UserProfileComponent implements OnInit {
  user: User;

  constructor( private usersNetworksService: UsersNetworksService,
               private location: Location) { }

  ngOnInit() {
    this.user = this.usersNetworksService.getCurrentUser();
  }

  back() {
    this.location.back();
  }
}
