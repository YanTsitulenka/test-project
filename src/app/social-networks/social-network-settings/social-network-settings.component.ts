import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-social-network-settings',
  templateUrl: './social-network-settings.component.html',
  styleUrls: []
})
export class SocialNetworkSettingsComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }
}
